const express = require("express");
const { createServer } = require("http");
const { Server } = require("socket.io");
const uuid = require('uuid');

const Message = require("./utils/message");
const User = require("./users");

const PORT = 9595 || process.env.PORT;

const app = express();
const server = createServer(app);
const io = new Server(server, {
  transports: ["websocket"],
  cors: {
    origin: "*",
    allowedHeaders: ["x-chat-service"],
  },
});

io.engine.generateId = (req) => {
  return uuid.v4();
}

const chat = io.of('/chat');

chat.on("connection", (socket) => {
  console.log(`Connected to client ${socket.id}`);

  socket.on("adduser", (userId) => {
    User.add({ userId: userId, socketId: socket.id });

    chat.emit('getOnlineUsers', User.all() );
  });

  socket.on("sendMessage", ({ receiverId, message }) => {
    const receiver = User.getByUserId(receiverId) || { userId: receiverId };

    const sender = User.getBySocketId(socket.id) || { userId: senderId };

    const messagedata = Message.format(sender.userId, message);

    const data = Message.save({ receiverId: receiver.userId, ...messagedata, isSeen: false });

    if (!!receiver.socketId) {
      chat.to(receiver.socketId).emit("receiveMessage", data);
    }

    socket.emit("receiveMessage", data);
  });

  socket.on("getConversation", ({ receiverId }) => {
    let sender = User.getBySocketId(socket.id);
    let messages = Message.getConversation({ senderId: sender.userId, receiverId: receiverId });
    chat.to(sender.socketId).emit("conversation", messages);
  });

  socket.on('disconnect', () => {
    User.remove(socket.id);

    chat.emit("getOnlineUsers", User.all());
  });

  socket.on("connect_error", (err) => {
    console.log(`connect_error due to ${err.message}`);
  });
});

server.listen(PORT, () => {
  console.log(`Chat server running on port ${PORT}`);
});