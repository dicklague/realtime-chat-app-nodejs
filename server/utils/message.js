const moment = require('moment');
const uuid = require('uuid');

const messages = [];

const Message = function () { };

Message.prototype.format = function (senderId, message) {
  return {
    senderId: senderId,
    message: message,
    timestamp: moment().valueOf(),
  };
};

Message.prototype.save = function ({ senderId, receiverId, message, timestamp, isSeen = true }) {
  const messageId = uuid.v4();
  const data = { messageId, senderId, receiverId, message, timestamp, isSeen };
  messages.push(data);
  return data;
}

Message.prototype.getById = function (messageId) {
  return messages.find((message) => String(message.messageId) === String(messageId));
}

Message.prototype.getConversation = function ({ senderId, receiverId }) {
  return messages.filter((message) => (message.senderId === senderId && message.receiverId === receiverId) || (message.senderId === receiverId && message.receiverId === senderId));
};

Message.prototype.all = function () {
  return messages;
};

module.exports = new Message();