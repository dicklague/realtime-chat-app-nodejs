let users = [];

let User = function () { };

User.prototype.add = function ({ userId, socketId }) {
  !users.some((user) => String(user.userId) === String(userId)) &&
    users.push({ userId, socketId });
};

User.prototype.remove = function (socketId) {
  users = users.filter((user) => String(user.socketId) !== String(socketId));
};

User.prototype.getByUserId = function (userId) {
  return users.find((user) => String(user.userId) === String(userId));
};

User.prototype.getBySocketId = function (socketId) {
  return users.find((user) => String(user.socketId) === String(socketId));
};

User.prototype.all = function () {
  return users;
}

module.exports = new User();